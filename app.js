// Load HTTP module
const http = require("http");

const hostname = '127.0.0.1';
const port = 8000;

const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.write('hello world!');
        res.end();
    }


    if (req.url === '/api/courses') {
        res.write(JSON.stringify([1,2,3]));
        res.end();
}
});

server.listen(port, hostname, function() {
    console.log(`server running at http://${hostname}:${port}/`);
});

