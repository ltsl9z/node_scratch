
// import joi for request validation and error handler
const Joi = require('joi');
// import express
const express = require("express");
// define app
const app = express();
app.use(express.json());

const courses = [
    {id : 1, name : 'course1'},
    {id : 2, name : 'course2'},
    {id : 3, name : 'course3'}
];

// get method
app.get( '/', (req, res) => {
    res.send('Hello world!')
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) res.status(404).send('the course of the given ID was not found');
    res.send(course);
});

// post
app.post('/api/courses', (req, res) => {
    const schema = {
        name : Joi.string().min(3).required()
    };
    const result = Joi.validate(req.body, schema);
    console.log(result);

    if (result.error) {
        res.status(400).send(result.error.details[0].message);
    };

    const course = {
        id : courses.length + 1,
        name : req.body.name
    };
    courses.push(course);
    res.send(course);
});

// port
const port = process.env.PORT || 3000;
app.listen(
    port, () => console.log(`listening on port ${port}...`)
)